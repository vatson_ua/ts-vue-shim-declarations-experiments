import Vue, { CreateElement } from "vue";
export default class Bar extends Vue {
    name: string;
    render(n: CreateElement): import("vue").VNode;
}
