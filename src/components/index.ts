import { Bar } from './Bar'
import { Foo } from './Foo'

export default {
  Bar,
  Foo,
}